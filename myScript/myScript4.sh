#!/bin/bash

echo "list of files"; read files 


for file in $files
do

  if [ ! -e "$file" ]       # Проверка наличия файла.
  then
    echo "Файл $file не найден."; echo
    continue                # Переход к следующей итерации.
  fi

  ls -l $file | awk '{ print $8 "         размер: " $5 }'  # Печать 2 полей.
  whatis `basename $file`   # Информация о файле.
  echo
done

exit 0
